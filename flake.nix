{
  description = "Website of MČR 2021";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    hakyll-contrib-tojnar.url = "gitlab:tojnar.cz/hakyll-contrib-tojnar";

    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-compat, hakyll-contrib-tojnar, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        haskellPackages = pkgs.haskellPackages.override {
          overrides = pkgs.lib.composeExtensions (final: prev: {
            mcr2021-rogaining-cz = final.callPackage ./mcr2021-rogaining-cz.nix { };
          }) hakyll-contrib-tojnar.haskellOverlay;
        };

      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            self.defaultPackage.${system}

            # For deploy
            pkgs.rclone
            pkgs.python3
          ];
        };

        haskellDevShell = self.packages.${system}.mcr2021-rogaining-cz.env.overrideAttrs (attrs: {
          nativeBuildInputs = attrs.nativeBuildInputs ++ [
            pkgs.cabal-install
          ];
        });

        packages.mcr2021-rogaining-cz = haskellPackages.mcr2021-rogaining-cz;

        defaultPackage = self.packages.${system}.mcr2021-rogaining-cz;

        apps.mcr2021-rogaining-cz = utils.lib.mkApp { drv = self.packages.${system}.mcr2021-rogaining-cz; };

        defaultApp = self.apps.${system}.mcr2021-rogaining-cz;
      }
  );
}
