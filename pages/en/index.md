---
title: News
author: Jan Tojnar
---
##### 6. 7. 2021
7 days after publishing the event [results](/en/results.html) are regarded as official.

##### 29. 6. 2021
Evebnt photos by organizers and participants are published in the event [photogallery](photos.html) and facebook:
[„Máme&nbsp;rádi&nbsp;rogaining“](https://www.facebook.com/groups/233765541508293).

##### 28. 6. 2021
The event is over, [results](/en/results.html) <!-- and [splits](/en/splits.html) --> are available on the web.<br>
On the Play-Map website you can upload your event routes (GPS logs) or draw your route manually:<br>

* [Czech Rogaining Champs 2021 - routes](http://play-map.com/event.php?id=530&lang=english)

##### 23. 6. 2021
The [Final Bulletin](https://mcr2021.rogaining.cz/en/final_bulletin_2021.html) has been released. See you in Křižánky.

##### 20. 6. 2021
We have marked all check points, the last map update is over. 
Unfortunately restaurants in Krizanky are open up to 20:00 only. Manage your Friday dinner on your way to the event centre or utilize your own food.

##### 17. 6. 2021
Tomorrow, on Friday June 18, the early bird registration is over. We will wait another few days for latecomers, but on Monday June 21 is the deadline for participation numbers announcement to the Zaket Publishing. As the event participation is very low this year, we can´t afford printing of outstanding maps.

##### 10. 6. 2021
Two new photo links added into the event [photogallery](photos.html).

##### 4. 6. 2021
[Bulletin](https://mcr2021.rogaining.cz/en/bulletin_2021.html) has been released.
Now you can enter via [entry system](https://entries.mcr2021.rogaining.cz/en/). 

##### 26. 5. 2021
We have visited the event area last weekend. Enjoy [photos](https://photos.app.goo.gl/xSKrMt7KCUJxDUfu5) of the marvelous Vysočina Highlands. 

##### 13. 5. 2021
Despite the COVID epidemic we are trying to organize the Czech Rogaining Championships 2021. Term: June 26 and 27, somewhere at Vysočina highlands. The event will be little constrained in order to maximize safety of all event participants. E.g. we are not able to run Hash House etc. 

The complete list of safety measures and event bulletin will be revealed soon. Entry system should be available June 1, 2021. The final decision about event holding or cancellation will be announced no later than June 21, 2021.

<!-- 
##### 29. 6. 2021
7 days for result review has been over, the [results](/en/results.html) are official now.<br>
We have published some event photos in the [photogallery](/en/photos.html).

##### 24. 6. 2021
[JPG of the event map](/files/Map_MCR_2021.jpg) for download is available (150 DPI, 2.4 MB).

##### 14. 6. 2021

Today at midnight the 1<sup>st</sup> registration phase will be terminated and tomorrow we are going to order printing of event maps with minimal number of reserve copies. As we haven´t received any foreign entry so far, we probably won´t translate Final Bulletin and event descriptions into English. But there are still few hours remaining...

##### 10. 6. 2021

The event map is ready to print, we have also prepared [diplomas for winners](/files/Diplom_XO.pdf).

##### 7. 6. 2021

We have marked all check points this weekend, have a look at several photos in [photogallery](photos.html).

##### 1. 6. 2021

The major part of the fieldwork has been accomplished this weekend, now it is time to release the [bulletin](bulletin_2021.html) and start [entry system](https://entries.mcr2021.rogaining.cz/en/).
-->
