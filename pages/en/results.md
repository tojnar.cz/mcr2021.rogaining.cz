---
title: Results
---

##### Czech Rogaining Championships 2021 - final results

* [Overall results](/files/results_all.pdf)
* [Splits](/en/splits.html)

##### Route drawing and GPS log upload

Please draw your racing routes or upload your GPS log on the Play-Map website:

* [Czech Rogaining Champs 2021 - routes](http://play-map.com/event.php?id=530&lang=english)
