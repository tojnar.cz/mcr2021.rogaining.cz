---
title: <!-- Final Bulletin of the Czech Rogaining Championships 2021 -->
---
<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
  <tbody>
    <tr>
      <td style="width:21%;">
        <center><a href="http://www.rogaining.cz/" target="_blank"><img src="/images/car.gif" alt="Logo ČAR"></a></center>
      </td>
      <td colspan="3" style="width:58%;" align="justify">
        <center><h4>Final Bulletin of the Czech Rogaining Championships 2021</h4></center>
      </td>
      <td style="width:21%;">
        <center><a href="http://www.caes.cz" target="_blank"><img src="/images/caes-cerna.gif" alt="Logo ČAES"></a></center>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Organizers:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Bratrstvo Žďárských vrchů,
        <br>
        and Krušnohorský rogainingový klub Litvínov.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Date:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <strong>26.–27.&nbsp;6. 2021</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event site:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <strong>Křižánky</strong>, Žďár nad Sázavou district, Czech Republic
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event Centre:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <a href="https://mapy.cz/s/lalubejavo" target="_blank">A playground near the village cemetery</a>; GPS:&nbsp;N49.6841542°, E16.0627439°.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event type and used map:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Czech Rogaining Champs 2021 (Saturday 12:00–Sunday 12:00)</li>
          <li>Prequalifying event for WRC 2021</li>
          <li>1<sup>st</sup> event of the ČAR series 2021</li>
          <li>12 - hour public race (Saturday 10:00–22:00)</li>
          <li>6 - hour public race (Saturday 13:00–19:00) </li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event area and terrain:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        The event area is bordered by municipalities of Hlinsko, Borová, Jimramov, Skalský Dvůr, Nové Město na Moravě, Vysoké and Vojnův Městec.<br>
        The Žďárské vrchy highlands, altitude 600–836&nbsp;m are moderately densely populated area with a dense network of roads and paths. The landscape is occasionally used for agriculture (fields and pastures).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event area issues:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Be careful on roads, there is heavy traffic even on weekends. Please mind it is obligatory to wear a reflective band if you are walking on a road outside residential area at night.</li>
          <li>There are many steep rock formations in the terrain, be careful not only at night, especially at check points 67, 72, 82 a 91.</li>
          <li>There are numerous preservation areas and quiet animal zones in the event area, marked by hatching in the map as restricted areas.<br>
          <strong>It is strictly forbidden to cross these areas through the forest. Follow the paths and roads marked on the map!</strong></li>
          <li>Orchards, gardens, pastures and other private areas with a risk of conflict with a bull or a landowner are marked by purple cross-hatching. Do not enter any other private area as well.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Map:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Special rogaining map, scale 1:&nbsp;40&nbsp;000, e&nbsp;=&nbsp;10&nbsp;m, condition 06/2021, printed on waterproof material (Pretex).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Special map symbols:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <a href="/files/Special_Symbols.jpg" target="_blank"><img src="/files/Special_Symbols.jpg" alt="Special symbols" width="350"></a>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Descriptions:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        The check points descriptions in Czech and English (including point values) will be distributed together with the competition map.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Terms of Participation:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        At the registration desk all competitors have to sign a declaration that they start at their own risk and they are:
        <br>
        i) COVID negative and out of any touch with any infected person, or
        <br>
        ii) recovered from COVID maximum 180 days ago, or
        <br>
        iii) it has been more than three weeks since completed vaccination (22 days). For participants under the age of 18, this statement has to be signed by their parent or a legal guardian.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Teams and categories:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        A team consists of 2–5 competitors. All team members must complete the entire race together.<br>
        You can submit your entry for the following categories:
        <ul>
          <li>24 - hour Czech Championships (MY, WY, XY, MO, WO, XO, MV, WV, XV, MSV, WSV, XSV)</li>
          <li>12 - hour public race (MO12, WO12, XO12)</li>
          <li>6 - hour public race (MO6, WO6, XO6)</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Transport:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        We strongly recommend individual car transport.<br>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Parking:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        The parking lot is near the event centre, parking will be regulated by the organizers. Entering the event centre playground by car is strictly prohibited!
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Accommodation:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Tent area will be designed at the event centre playground, see map:<br><a href="/files/Event_Centre.jpg" target="_blank"><img src="/files/T_Event_Centre.jpg" alt="Event Centre" width="350"></a> The accommodation is free of charge for  12 - and 24 - hour event participants.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event centre catering:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Due to the COVID 19 restrictions, organizers will provide only limited hash house service and organizer´s bistro. Use your own dishes and cutlery to minimize waste.
        <br>
        There are several restaurants in the village offering meals, but they close at 20:00. Manage your dinner along the way or eat your own meal.
        <br>
        A village café and groceries should be open on Saturday morning.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event refreshment:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Drinking water will be provided in the event centre. There are numerous springs at the event area. You can also utilize water from streams, but make sure the upstream pasture doesn´t pollute the water.
        <br>
        The event area offers relatively high number of restaurants. These are marked on the map, but we do not know whether they will be open.
        You can make use of "Vorel´s beer pit" - a buried box full of beers and nonalcoholic beverages. Just pay 20 CZK into the money box inside and drink a beer or another beverage.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Hygienic measures, WC and washing:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        If you are showing signs of an illness, please stay at home. Keep the appropriate distance in the event centre.<br>
        You can use mobile toilets equipped with disinfection.<br>
        The event centre is located in Žďárské vrchy conservation area, please take all your garbage home or dispose it in the organizer´s plastic bins.<br>
        Use the adjacent pond for washing.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>24 hour rogaine event schedule:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
          <strong>Friday June 25:</strong><br>
          18:00–22:00 – registration of participants
        </p>
        <p>
          <strong>Saturday June 26:</strong><br>
          7:00–9:00 – registration of participants (incl. chip sealing)<br>
          10:00 – end of the chip sealing, start of map handout<br>
          12:00 – start of the race<br>
        </p>
        <p>
          <strong>Sunday June 27:</strong><br>
          12:00 – end of the race<br>
          after 14:00 – expected start of prize giving ceremony
        </p>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>12 hour rogaine event schedule:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
          <strong>Friday June 25:</strong><br>
          18:00–22:00 – registration of participants
        </p>
        <p>
          <strong>Saturday June 26:</strong><br>
          7:00–9:00 – registration of participants (incl. chip sealing)<br>
          9:00 – end of the chip sealing, start of map handout<br>
          10:00 – start of the race<br>
          22:00 – end of the race<br>
          after 22:30 – expected start of prize giving ceremony
        </p>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>6 hour rogaine event schedule:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
          <strong>Saturday June 26:</strong><br>
          7:00–11:00 – registration of participants (incl. chip sealing)<br>
          12:00 – end of the chip sealing, start of map handout<br>
          13:00 – start of the race<br>
          19:00 – end of the race<br>
          after 20:00 – expected start of prize giving ceremony
        </p>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Punching system:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Electronic (SportIdent), each team member must have his/her own SI chip! For 12 and 24-hour races we recommend only chips with increased capacity. If you sign up with a lower capacity chip (30 checks for SI 5 and SI 8 or 50 control SI 9), we will not count any checks beyond those recorded in the chip. All SI stations will operate in AIR+ mode.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event course and team evaluation:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <p>
          Control points will have a numeric code – both on the map and in the control descriptions. When you multiply the first digit of the code by ten, you´ll get the point value of that particular control point (minimum 30, maximum 90 points). It is up to you how many control points and in which order you visit.<br>
          A control point will be marked with regular orienteering white-orange flag 30×30&nbsp;cm, SI unit with code of the control and red and white ribbon. When punching please wait for acoustic and visual signal of the SI unit. If the SI unit does not work please write down a letter code from the red and white ribbon.<br>
          Final ranking of teams will depend on the total score achieved. When the points are equal, the lower target time decides. If the time limit is exceeded, 20 points will be deducted for each minute of delay. If the time limit is exceeded by 30 minutes or more, the team will not be evaluated.<br>
          The best 2 teams in MO, XO and WO categories are automatically prequalified to the WRC 2021. Winning teams in Open categories gain a certificate of free start at&nbsp;the&nbsp;2<sup>nd</sup> event of the&nbsp;ČAR series 2021, <a href="https://bloudeni.krk-litvinov.cz/2021/en/" target="_blank">Lost&nbsp;in&nbsp;Tojnarek’s&nbsp;Sudetenland</a> (team has to compete at the same team composition).
        </p>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Event officials:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Event director: Miroslav Seidl<br>
        Event Course setters: Zdeněk Rajnošek, Miroslav Seidl<br>
        Event Referee: Jan Tojnar
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Notice:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Participation in the rogaining event is at the sole risk of each competitor. All participants must sign an indemnity form at the registration desk.</li>
          <li>The organizer is not responsible or liable for damages caused by any participant of the race.</li>
          <li>Mandatory equipment – a whistle and reflective visibility bands for movements after dark on roads.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>