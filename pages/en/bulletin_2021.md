---
title: <!-- Bulletin of the Czech Rogaining Championships 2021 -->  
---
<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
  <tbody>
    <tr>
      <td style="width:21%;">
          <center><a href="http://www.rogaining.cz/" target="_blank"><img src="/images/car.gif" alt="Logo ČAR"></a></center>
      </td>
      <td colspan="3" style="width:58%;" align="justify">
          <center><h4>Bulletin of the Czech Rogaining Championships 2021</h4></center>
      </td>
      <td style="width:21%;">
          <center><a href="http://www.caes.cz" target="_blank"><img src="/images/caes-cerna.gif" alt="Logo ČAES"></a></center>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Organizers:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Bratrstvo Žďárských vrchů,
          <br>
          and
          <br>
          Krušnohorský rogainingový klub Litvínov.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Date:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>26.–27.&nbsp;6. 2021</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event site:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>Křižánky</strong>, Žďár nad Sázavou district, Czech Republic
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event Centre:</strong>
      </td>
      <td colspan="4" style="width:79%;">
       <a href="https://mapy.cz/s/lalubejavo" target="_blank">A playground near the village cemetery</a>; GPS:&nbsp;N49.6841542°, E16.0627439°.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event type and used map:</strong>
      </td>
      <td colspan="4" style="width:79%;">               
         <ul>
         <li>Czech Rogaining Champs 2021 (Saturday 12:00–Sunday 12:00)</li>
         <li>Prequalifying event for WRC 2021</li>
         <li>1<sup>st</sup> event of the ČAR series 2021</li>
         <li>12 - hour public race (Saturday 10:00–22:00)</li>
         <li>6 - hour public race (Saturday 13:00–19:00) </li>
         </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event area and terrain:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      	The event area is bordered by municipalities of Hlinsko, Borová, Jimramov, Skalský Dvůr, Nové Město na Moravě, Vysoké and Vojnův Městec.<br>
        The Žďárské vrchy highlands, altitude 600–836&nbsp;m are moderately densely populated area with a dense network of roads and paths. The landscape is occasionally used for agriculture (fields and pastures). There are numerous preservation areas and quiet animal zones in the event area, hatched in the map as restricted areas.<br>
        <strong>Crossing of these areas through forest is strictly prohibited, keep the paths and roads marked in the map!</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Map:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Special rogaining map, scale 1:&nbsp;40&nbsp;000, e&nbsp;=&nbsp;10&nbsp;m, condition 06/2021, printed on waterproof material (Pretex).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Terms of Participation:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        At the registration desk all competitors have to sign a declaration that they start at their own risk and they are COVID negative and out of any touch with any infected person, or recovered from COVID max. 180 days ago, or have completed vaccination not later than 22 day ago. For participants under the age of 18, this statement has to be signed by their parent or legal guardian.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Teams and categories:</strong>
    </td>
    <td colspan="4" style="width:79%;">
      A team consists of 2–5 competitors. All team members must complete the entire race together.<br>
      You can submit your entry for the following categories:
        <ul>
        <li>24 - hour Czech Championships (MY, WY, XY, MO, WO, XO, MV, WV, XV, MSV, WSV, XSV)</li>
        <li>12 - hour public race (MO12, WO12, XO12)</li> 
        <li>6 - hour public race (MO6, WO6, XO6)</li>
        </ul>
    <strong>Specification of abbreviations:</strong> <br>
        <ul>
        <li><strong>M</strong> – men only</li>
        <li><strong>W</strong> – women only</li>
        <li><strong>X</strong> – at least one man and at least one woman</li>
        <li><strong>Y</strong> – youth – all team members must be up to 23 or younger on the start day, i.e. born on June 26, 1998 or later</li>
        <li><strong>O</strong> – open – no age limitation</li>
        <li><strong>V</strong> – veterans – all team members must be min. 40 years, i.e. born on June 26,&nbsp;1981 or earlier</li>
        <li><strong>SV</strong> – superveterans – all team members must be min. 55 years, i.e. born on June 26,&nbsp;1966 or earlier</li>
      </ul>
    </td>
  </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Punching system:</strong>
      </td>
      <td colspan="4" style="width:79%;">
   		Electronic (SportIdent), each team member must have his/her own SI chip! For 12 and 24-hour races we recommend only chips with increased capacity. If you sign up with a lower capacity chip (30 checks for SI 5 and SI 8 or 50 control SI 9), we will not count any checks beyond those recorded in the chip. You can borrow high-capacity chips at the price of&nbsp;CZK&nbsp;100 from the organizer, we only accept orders via an electronic registration system. Your chips will wait for you at the registration desk.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Entries:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      Via <a href="https://entries.mcr2021.rogaining.cz/en/" target="_blank">on-line entry system</a>.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Entry fee:</strong>
      </td>
      <td colspan="4" style="width:79%;">
  <table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
    <tbody>
      <tr>
        <td><strong>Event type</strong></td><td><strong>Basic entry fee per 1 person</strong></td><td><strong>Late entry fee per 1 person</strong></td>
      </tr>
      <tr>
        <td><strong>6 hour</strong></td><td>300&nbsp;CZK</td><td>400&nbsp;CZK</td>
      </tr>
      <tr>
        <td><strong>24 a 12 hour</strong></td><td>600&nbsp;CZK</td><td>750&nbsp;CZK</td>
      </tr>
    </tbody>
  </table><br>
    Basic entry fee have to be paid up to Friday June&nbsp;18&nbsp;2021&nbsp;24:00. From Saturday June&nbsp;19&nbsp;we will accept only limited number of entries for increased starting fee. Entry fee includes racing maps and descriptions, free parking, accommodation in tents (from Friday to Sunday for 12 and 24 hour rogainers), social facilities, and supply of fresh water at the event centre.
    </td>
    </tr>
    <tr>
        <td style="width:21%;" valign="top">
            <strong>Payment:</strong>
        </td>
        <td colspan="4" style="width:79%;">
              Starting fee and, or SI chip rental pay to the bank account of Czech Rogaining Association.<br>
              Bank address: Česká spořitelna, a.s., Olbrachtova 1929/62, 140 00 Praha 4<br>
              IBAN: CZ8208000000000721206359<br>
              BIC/SWIFT code: GIBACZPX
        </td>
        </tr>
    <tr>
    <td style="width:21%;" valign="top">
            <strong>Transport:</strong>
    </td>
        <td colspan="4" style="width:79%;">
        We strongly recommend individual car transport.<br>
     </td>
     </tr>
     <tr>
          <td style="width:21%;" valign="top">
              <strong>Parking:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          The parking lot nearby the event centre, parking will be regulated by the organizers. Entering the event centre playground by car is strictly prohibited!
      </td>
      </tr>
       <tr>
       <td style="width:21%;" valign="top">
            <strong>Catering:</strong>
        </td>
        <td colspan="4" style="width:79%;">
        	Due to the COVID 19 restrictions organizers will provide only limited hash house service and organizer´s bistro. More detailed info will be revealed later on the event website.
        <br>
        There is several meal offering restaurants in the village, we will inform you whether they are open.
        </td>
      	</tr>
    	<tr>
      	<td style="width:21%;" valign="top">
         <strong>24 hour rogaine event schedule:</strong>
	     </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
        <strong>Friday June 25:</strong><br>
        18:00–22:00 – registration of participants
        </p>
        <p>
        <strong>Saturday June 26:</strong><br>
        7:00–9:00 – registration of participants (incl. chip sealing)<br>
        10:00 – end of the chip sealing, start of map handout<br>
        12:00 – start of the race<br>
        </p>
        <p>
        <strong>Sunday June 27:</strong><br>
        12:00 – end of the race<br>
        after 14:00 – expected start of prize giving ceremony
        </p>
      </td>        
    </tr>
    <tr>    
      <td style="width:21%;" valign="top">
          <strong>12 hour rogaine event schedule:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
        <strong>Friday June 25:</strong><br>
        18:00–22:00 – registration of participants
        </p>
        <p>
        <strong>Saturday June 26:</strong><br>
        7:00–9:00 – registration of participants (incl. chip sealing)<br>
        9:00 – end of the chip sealing, start of map handout<br>
        10:00 – start of the race<br>
        22:00 – end of the race<br>
        after 22:30 – expected start of prize giving ceremony
        </p>
      </td> 
    </tr>
    <tr>         
      <td style="width:21%;" valign="top">
          <strong>6 hour rogaine event schedule:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
        <strong>Saturday June 26:</strong><br>
        7:00–11:00 – registration of participants (incl. chip sealing)<br>
        12:00 – end of the chip sealing, start of map handout<br>
        13:00 – start of the race<br>
        19:00 – end of the race<br>
        after 20:00 – expected start of prize giving ceremony
        </p> 
        </td>
    </tr>
    <tr>
        </td>
	   </tr>
	   <tr>
      	<td style="width:21%;" valign="top">
          		<strong>Event course and team evaluation:</strong>
      	</td>
      	<td colspan="4" style="width:79%;">
      	<p>Control points will have numeric code – both on the map and in the control descriptions. When you multiply the first digit of the code by ten, you´ll get the point value of that particular control point (minimum 30, maximum 90 points). It is up to you how many control points and in which order you visit.<br>
        A control point will be marked with regular orienteering white-orange flag 30×30&nbsp;cm, SI unit with code of the control and red and white ribbon. When punching please wait for acoustic and visual signal of the SI unit. If the SI unit does not work please write down a letter code from the red and white ribbon.<br>
        Final ranking of teams will depend on the total score achieved. When the points are equal, the lower target time decides. If the time limit is exceeded, 20 points will be deducted for each minute of delay. If the time limit is exceeded by 30 minutes or more, the team will not be evaluated.<br>
        The best 2 teams in MO, XO and WO categories are automatically prequalified to the WRC 2021. Winning teams in Open categories gain a certificate of free start at&nbsp;the&nbsp;2<sup>nd</sup> event of the&nbsp;ČAR series 2021, <a href="https://bloudeni.krk-litvinov.cz/2021/en/" target="_blank">Lost&nbsp;in&nbsp;Tojnarek’s&nbsp;Sudetenland</a> (team has to compete at the same team composition).</p>
      	</td>
    	</tr>
      <tr>
        <td style="width:21%;" valign="top">
              <strong>Event officials:</strong>
        </td>
        <td colspan="4" style="width:79%;">
            Event director: Miroslav Seidl<br>
            Event Course setters: Zdeněk Rajnošek, Miroslav Seidl<br>
            Event Referee: Jan Tojnar
        </td>
      </tr>
    	<tr>
      	<td style="width:21%;" valign="top">
          		<strong>Notice:</strong>
      	</td>
      	<td colspan="4" style="width:79%;">
	          <ul>
	          <li>Taking part in the rogaining event is on the own risk of every competitor. All participants have to sign an indemnity form at the registration desk.</li>
	          <li>The organizer doesn’t hold responsibility and/or liability for any damage caused by any participant of the race.</li>
            <li>Compulsory equipment – a whistle and reflective visibility bands for after dark movement on roads.</li>
	          </ul>
      	</td>
    	</tr>
  		</tbody>
		</table>