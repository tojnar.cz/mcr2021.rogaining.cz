---
title: Spilts CZRC 2021
author: Jan Tojnar
---

* [01 – coury.cz záskok](/files/splits/01.pdf)
* [02 – Cookies &amp; Pancakes](/files/splits/02.pdf)
* [03 – Krušnohorský Expres](/files/splits/03.pdf)
* [04 – Seskočil z rudé skály a vyrazil si dech](/files/splits/04.pdf)
* [05 – Košířské šlapky](/files/splits/05.pdf)
* [06 – FraPa](/files/splits/06.pdf)
* [07 – Kandjoktjo](/files/splits/07.pdf)
* [08 – Don Quijote](/files/splits/08.pdf)
* [09 – Kolmo na vrstevnice!](/files/splits/09.pdf)
* [10 – WITaj](/files/splits/10.pdf)
* [11 – WITaj Baby](/files/splits/11.pdf)
* [12 – FBA team](/files/splits/12.pdf)
* [13 – Ať žijí mimiňáci](/files/splits/13.pdf)
* [14 – Věříšovi](/files/splits/14.pdf)
* [15 – SOOBí KRK](/files/splits/15.pdf)
* [16 – Kadlecovi](/files/splits/16.pdf)
* [17 – OB Říčany](/files/splits/17.pdf)
* [18 – Čerpa](/files/splits/18.pdf)
* [19 – Není kam spěchat](/files/splits/19.pdf)
* [20 – Počkaj na mňa](/files/splits/20.pdf)
* [21 – Gazely ze Žižkova](/files/splits/21.pdf)
* [22 – Plavoj](/files/splits/22.pdf)
* [23 – Bludné balvany](/files/splits/23.pdf)
* [24 – Lochnesky](/files/splits/24.pdf)
* [25 – Lochnesky a Sýček jdou na výletíček](/files/splits/25.pdf)
* [26 – MaPa](/files/splits/26.pdf)
* [27 – Losíci](/files/splits/27.pdf)
* [28 – Hoptrop](/files/splits/28.pdf)
* [29 – ToTo jede](/files/splits/29.pdf)
* [30 – EM team](/files/splits/30.pdf)
* [31 – Kuřata a slimák](/files/splits/31.pdf)
* [32 – Jahody v betonu](/files/splits/32.pdf)
* [33 – TAJFUN Litomyšl / OB Kotlářka](/files/splits/33.pdf)
* [34 – Nejkulaťoulinkatější πr^2](/files/splits/34.pdf)
* [35 – MGM](/files/splits/35.pdf)
* [36 – Běhej Poděbrady](/files/splits/36.pdf)
* [37 – Šternberské hvězdy](/files/splits/37.pdf)
* [38 – OK Tesil](/files/splits/38.pdf)
* [39 – Štika a Drak eXtreme Team](/files/splits/39.pdf)
* [40 – OB Chrast (#botasseblizi)](/files/splits/40.pdf)
* [41 – Stříbrná svatba](/files/splits/41.pdf)
* [42 – Historici](/files/splits/42.pdf)
* [43 – Krokouši](/files/splits/43.pdf)
* [44 – Píďalky](/files/splits/44.pdf)
* [45 – GiL Poland](/files/splits/45.pdf)
* [46 – KUA, to je kopec !](/files/splits/46.pdf)
* [48 – Jeleni](/files/splits/48.pdf)
* [49 – Bludičky s medvědem](/files/splits/49.pdf)
* [50 – Brambory](/files/splits/50.pdf)
* [51 – Run The World](/files/splits/51.pdf)
* [52 – PoloMáci](/files/splits/52.pdf)
* [53 – Valaši revival](/files/splits/53.pdf)
* [54 – Na limit](/files/splits/54.pdf)
* [55 – KUAKUO](/files/splits/55.pdf)
* [56 – Mauersegler Berlin](/files/splits/56.pdf)
* [57 – Nutný zlo](/files/splits/57.pdf)
* [58 – špunti z Přepalova](/files/splits/58.pdf)
* [59 – a spol.](/files/splits/59.pdf)
* [60 – Krizáci](/files/splits/60.pdf)
* [61 – Don't worry be happy](/files/splits/61.pdf)
* [62 – Alpsport](/files/splits/62.pdf)
* [63 – Krasotinky](/files/splits/63.pdf)
* [64 – Quo Vadis (solo out of competition)](/files/splits/64.pdf)
* [65 – 100klaßa](/files/splits/65.pdf)
* [66 – chromajzl &amp; vožungr](/files/splits/66.pdf)
* [67 – Klusík](/files/splits/67.pdf)
* [69 – Hyeny bez koček](/files/splits/69.pdf)
* [70 – Skauti Rychnov](/files/splits/70.pdf)
* [71 – Tvoje máma](/files/splits/71.pdf)
* [72 – Bratři na stopované](/files/splits/72.pdf)
* [73 – Stíhačky dorost 2](/files/splits/73.pdf)
* [74 – Hafík](/files/splits/74.pdf)
* [75 – Se hledá](/files/splits/75.pdf)
* [76 – Lopourovic](/files/splits/76.pdf)
* [77 – Rybízák](/files/splits/77.pdf)
* [79 – Holky Myslivcovy](/files/splits/79.pdf)
* [80 – Horníci z Adršpachu](/files/splits/80.pdf)
* [82 – Ztracené existence](/files/splits/82.pdf)
* [83 – Roztočtí tygři](/files/splits/83.pdf)
* [84 – Vyslanci Larse Heletága](/files/splits/84.pdf)
* [100 – Jan Kotyk (solo out of competition)](/files/splits/100.pdf)
