[![ČAR](/images/car.gif)](http://www.rogaining.cz/)[![Event logo](/files/Logo_4_Palice.png){width=140}](https://mcr2021.rogaining.cz/en/)

[![Český web](/images/csflag.png)](/cs/)

#### Menu

* [News](/en/)
* [Results](results.html)
* [Route drawing](http://play-map.com/event.php?id=530&lang=english)
* [Photogallery](photos.html)
* [Final Bulletin](final_bulletin_2021.html)
* [Bulletin](bulletin_2021.html)
* [Entries](https://entries.mcr2021.rogaining.cz/en/)
* [Weather](weather.html)

#### Event partners

[![Křižánky village](/images/Logo_Krizanky.jpg){width=120}](http://www.obeckrizanky.cz/) [![Czech Republic Forestery](/images/Logo_LCR.jpg){width=120}](https://lesycr.cz/en/)

[![O-Mapy](/images/O-Mapy.jpg)](https://www.facebook.com/omapy.cz)   [![Žaket – Map printing](/images/Zaket.gif)](https://zaket.cz/)

[![Lucifer Headlamps](/images/LogoLucifer.png)](https://www.luciferlights.net/)   [![Playmap – route drawing](/images/playmap.png)](http://play-map.com/)

[![Bernard Brewery](/images/Bernard_40_8000.jpg){width=120}](https://www.bernard.cz/)   [![Marek & Marek Bakery Polička](/images/Marek_Marek_EN.jpg)](https://pekarstvi-marek-marek-sro.business.site/)
