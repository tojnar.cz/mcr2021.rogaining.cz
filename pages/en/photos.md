---
title: Photogallery
---
<!--
-->
<br>

##### Photos from the event weekend

* [Before the start](https://photos.app.goo.gl/yttZrZkFm31Exovb6)
* [Event photos by Sadlo](/en/fota-sadlo.html)
* [Before start by Cookies &amp; Pancakes](https://www.facebook.com/groups/233765541508293/posts/243035813914599/)
* [Prize giving ceremony by Cookies &amp; Pancakes](https://www.facebook.com/dan.dvoracek/posts/10159432497744592)

##### Photos from event preparation

* [Tojnárek 05/2021](https://photos.app.goo.gl/xSKrMt7KCUJxDUfu5)
* [Tojnárek 06/2021](https://photos.app.goo.gl/GWUsmAnrHXDbzYTp8)
* [Survey photos by Sadlo](/cs/sadlofotky-priprava.html)
