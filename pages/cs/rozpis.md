---
title: <!-- Rozpis 25. Mistrovství České republiky v rogainingu -->
---
<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
  <tbody>
    <tr>
      <td style="width:21%;">
          <center><a href="http://www.rogaining.cz/" target="_blank"><img src="/images/car.gif" alt="Logo ČAR"></a></center>
      </td>
      <td colspan="3" style="width:58%;" align="justify">
          <center><h4>Rozpis 25. Mistrovství České republiky v&nbsp;rogainingu</h4></center>
      </td>
      <td style="width:21%;">
          <center><a href="http://www.caes.cz" target="_blank"><img src="/images/caes-cerna.gif" alt="Logo ČAES"></a></center>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořádající orgán:</strong>
      </td>
      <td colspan="4" style="width:79%;">
           <a href="http://www.rogaining.cz/" target="_blank">Česká asociace rogainingu a horského orientačního běhu (ČAR)</a>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořádající subjekty:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Bratrstvo Žďárských vrchů a
          <br>
          Krušnohorský rogainingový klub Litvínov.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Datum:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>26.–27. červen 2021</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Místo:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>Křižánky</strong>, okres Žďár nad Sázavou, Česká republika.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Centrum závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <a href="https://mapy.cz/s/lalubejavo" target="_blank">Hřiště u hřbitova</a>; GPS:&nbsp;N49.6841542°, E16.0627439°.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Druh závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">               
         <ul>
         <li>Mistrovství České republiky v&nbsp;rogainingu na&nbsp;24&nbsp;hodin (sobota 12:00 hod. až neděle 12:00 hod.)</li>
         <li>Předkvalifikační závod pro&nbsp;MS v&nbsp;rogainingu v&nbsp;roce 2022</li>
         <li>1. závod ČAR série 2021</li>
         <li>Veřejný rogainingový závod na 12 hodin (sobota 10:00 až 22:00 hod.)</li>
         <li>Veřejný rogainingový závod na 6 hodin (sobota 13:00 až 19:00 hod.)</li>
         </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Prostor závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      	Prostor závodu je vymezen obcemi Hlinsko, Borová, Jimramov, Skalský Dvůr, Nové Město na Moravě, Vysoké a Vojnův Městec.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Terén:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Zvlněná krajina pahorkatiny Žďárských vrchů, nadmořská výška 600–836&nbsp;m. Středně hustě osídlená oblast s rozsáhlou sítí dopravních komunikací a cest. Krajina je místy zemědělsky využívána (pole, pastviny...). V&nbsp;závodním prostoru se nachází velký počet rezervací a klidových zón zvěře. V&nbsp;mapě budou vyšrafovány jako zakázaný prostor, průchod přes ně je povolen pouze po&nbsp;cestách znázorněných v&nbsp;mapě.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Mapa:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Speciálka pro rogaining, měřítko 1:&nbsp;40&nbsp;000, e&nbsp;=&nbsp;10&nbsp;m, stav 06/2021, vodovzdorně upravená (Pretex).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Podmínky účasti:</strong>
      </td>
      <td colspan="4" style="width:79%;">
         <ul>
          <li>Všichni závodníci budou na prezentaci podepisovat prohlášení, že startují na&nbsp;vlastní nebezpečí, nejeví známky nakažení COVID 19, ani nepřišli do&nbsp;styku s&nbsp;osobou infikovanou nebo v&nbsp;karanténě, popř. onemocnění COVID prodělali před maximálně 180 dny, či mají ukončené očkování nejpozději před 22 dny. Za&nbsp;účastníky mladší 18 let toto prohlášení podepíše jejich rodič nebo zákonný zástupce. Stáhněte si příslušný <a href="/files/souhlas_18.pdf" target="_blank">formulář</a></li>
          <li>Závodníci mladší 15 let se mohou zúčastnit pouze v&nbsp;doprovodu osoby starší 18&nbsp;let</li>
          <li>MČR na 24 hodin se smí zúčastnit pouze sportovci registrovaní v&nbsp;ČAR (týká se všech členů týmu). Bezplatná registrace je možná na&nbsp;prezentaci.</li>
          </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Týmy a kategorie:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Tým tvoří 2–5 závodníků. Všichni členové týmu musí absolvovat celý závod pohromadě. Při vzdání kteréhokoli člena družstva se musí kompletní tým dostavit do cíle a závod společně ukončit.<br>
        Přihlásit se můžete do&nbsp;následujících kategorií:
        <ul>
        <li>MČR na 24 hodin (MY, WY, XY, MO, WO, XO, MV, WV, XV, MSV, WSV, XSV)</li>
        <li>Veřejný závod na 12 hodin (MO12, WO12, XO12)</li> 
        <li>Veřejný závod na 6 hodin (MO6, WO6, XO6)</li>
        </ul>
    <strong>Upřesnění zkratek:</strong> <br>
        <ul>
        <li><strong>M</strong> – tým tvoří výhradně muži</li>
        <li><strong>W</strong> – tým tvoří výhradně ženy</li>
        <li><strong>X</strong> – v&nbsp;týmu je alespoň jeden muž a alespoň 1&nbsp;žena</li>
        <li><strong>Y</strong> – kategorie Youth – všichni členové týmu musí být v&nbsp;den startu ve věku 23 let nebo mladší, tj. narozeni 26.&nbsp;6.&nbsp;1998 či později</li>
        <li><strong>O</strong> – open – bez omezení věku</li>
        <li><strong>V</strong> – veteráni – všem členům týmu musí být v&nbsp;den startu min. 40 let, tj. narození 26.&nbsp;6.&nbsp;1981 či dříve</li>
        <li><strong>SV</strong> – superveteráni – všem členům týmu musí být v&nbsp;den startu min. 55 let, tj. narozeni 26.&nbsp;6.&nbsp;1966 či dříve</li>
      </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Systém ražení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Elektronický systém SportIdent.<br>
        <strong>!!! Každý člen týmu musí mít svůj SI čip !!!</strong><br>
        Pro závody na 12 a 24 hodin doporučujeme použít pouze čipy se zvýšenou kapacitou. Pokud se přihlásíte s&nbsp;nízkokapacitním čipem (30 kontrol u&nbsp;SI&nbsp;5&nbsp;a&nbsp;8 nebo 50 kontrol u&nbsp;SI&nbsp;9), nezapočítáme vám žádné kontroly kromě těch zaznamenaných v&nbsp;čipu. Vysokokapacitní čipy si můžete za&nbsp;cenu 100&nbsp;Kč zapůjčit od&nbsp;pořadatele, objednávky přijímáme výhradně prostřednictvím elektronického přihláškového systému. Čip pro vás bude připraven na&nbsp;prezentaci.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Přihlášky:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Výhradně pomocí <a href="https://entries.mcr2021.rogaining.cz/cs/" target="_blank">on-line přihláškového systému</a> na&nbsp;webu závodu.<br>
        Pro&nbsp;účely vytvoření registru sportovců ČAR bude od&nbsp;závodníků z&nbsp;České republiky systém vyžadovat vyplnění místa trvalého pobytu. Státní orgány budou časem ověřovat soulad registrovaného bydliště s&nbsp;adresou uvedenou v&nbsp;občance.<br>
        Vaše přihláška je zaregistrována v&nbsp;okamžiku, kdy kontaktní osoba (první člen týmu v&nbsp;přihláškovém formuláři) obdrží potvrzení (reply).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Startovné:</strong>
      </td>
      <td colspan="4" style="width:79%;">
  <table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
    <tbody>
      <tr>
        <td><strong>Druh závodu</strong></td><td><strong>Základní startovné za&nbsp;1&nbsp;osobu</strong></td><td><strong>Zvýšené startovné za&nbsp;1&nbsp;osobu</strong></td>
      </tr>
      <tr>
        <td><strong>6 hodin</strong></td><td>300&nbsp;Kč</td><td>400&nbsp;Kč</td>
      </tr>
      <tr>
        <td><strong>24 a 12 hodin</strong></td><td>600&nbsp;Kč</td><td>750&nbsp;Kč</td>
      </tr>
    </tbody>
  </table><br>
    Základní startovné platí v případě přihlášení a zaplacení do <strong>pátku 18.&nbsp;6.&nbsp;2021 24:00 hod.</strong><br>
    Od&nbsp;<strong>soboty 19.&nbsp;6.&nbsp;2021</strong> až do&nbsp;skončení prezentace budeme přihlášky přijímat jen omezeně (dokud nedojdou mapy) a za&nbsp;zvýšené startovné.<br>
    <strong>Startovné pro závody na&nbsp;12 a 24 hodin zahrnuje:</strong> tisk závodních map a popisů, bezplatný pobyt v&nbsp;centru závodu (pátek až neděle) včetně parkování, noclehu ve&nbsp;vlastních stanech, využití sociálního zařízení, šetrný odběr pitné vody a základní občerstvení při&nbsp;závodě v&nbsp;centru závodu (hash house) a po&nbsp;závodě.<br>
    <strong>Startovné pro závod na 6 hodin zahrnuje:</strong> tisk závodních map a popisů, parkovné, využití sociálního zařízení, šetrný odběr pitné vody a základní občerstvení po&nbsp;závodě.
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Způsob platby:</strong>
      </td>
     <td colspan="4" style="width:79%;">
        Abychom se vyhnuli placení v&nbsp;hotovosti na&nbsp;prezentaci, prosíme o&nbsp;uhrazení vložného (startovné + půjčovné za&nbsp;čipy) bankovním převodem na&nbsp;účet ČAR, vedený u&nbsp;České spořitelny pod&nbsp;číslem: <strong>721206359/0800</strong><br>
        Jako variabilní symbol platby uvádějte ID vašeho týmu, které naleznete v&nbsp;mailu potvrzujícím přijetí vaší přihlášky.<br>
        <strong>Pozor, tento email přijde pouze kontaktní osobě týmu, tj. tomu, kdo je v&nbsp;přihlášce uveden jako první!</strong>
        <br>
        V případě platby zaslané na&nbsp;poslední chvíli předložte na&nbsp;prezentaci kopii dokladu o&nbsp;zaplacení.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Doprava:</strong>
      </td>
      <td colspan="4" style="width:79%;">
		Vlastními dopravními prostředky nebo hromadnou dopravou, pořadatel dopravu nezajišťuje.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">    
    <strong>Parkování:</strong>
	 </td>
	 <td colspan="4" style="width:79%;">
		Pouze na vymezených plochách v&nbsp;blízkosti centra, bude značeno a řízeno pořadateli. Do&nbsp;areálu závodního centra je vjezd přísně zakázán.
	 </td>
	   </tr>
    <tr>
      <td style="width:21%;" valign="top">    
    <strong>Ubytování:</strong>
   </td>
   <td colspan="4" style="width:79%;">
    Závodníci na 12 a 24 hodin mají nárok na&nbsp;nocleh zdarma, ve&nbsp;vlastních stanech a ve&nbsp;vymezeném prostoru v&nbsp;centru závodu.<br>
    Ostatní zájemci o&nbsp;přespání (účastníci závodu na&nbsp;6&nbsp;hodin nebo nesoutěžící doprovod) musí na&nbsp;prezentaci uhradit poplatek 80&nbsp;Kč/stan a noc, případně mohou využít okolních ubytovacích kapacit v&nbsp;obci a okolí bez&nbsp;asistence pořadatele.
   </td>
     </tr>
      <tr>
      <td style="width:21%;" valign="top">
      <strong>Stravování a občerstvení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      V obci a jejím okolí je několik restaurací a stravovacích zařízení, která snad po&nbsp;rozvolnění opatření již budou opět v&nbsp;provozu. Podrobnější informace o&nbsp;dostupnosti stravování v&nbsp;obci a servisu v&nbsp;centru a během závodu postupně upřesníme na&nbsp;stránkách závodu. Nájezd provozu restaurací v&nbsp;okolí závodního centra budeme monitorovat, co zjistíme, zveřejníme :-).<br>
      V centru závodu na hřišti bude během akce fungovat základní bufet s&nbsp;občerstvením min. v&nbsp;rozsahu a sortimentu jako loňský rok (točené pivo, nealko pivo, nealko nápoje, káva, čaj).<br>
      V průběhu závodů na&nbsp;12 a 24 hodin bude v&nbsp;podobném rozsahu zajištěn základní hash house. Podrobnosti o&nbsp;sortimentu budou upřesněny.<br>
      Závodní prostor oplývá relativně velkým počtem restaurací a hospod. Budou zakresleny do&nbsp;mapy s&nbsp;tím, že funkčnost podniku nezaručujeme. Závodníci mohou kromě nich využít i zvláštní občerstvovačku (pivo nebo nealko za&nbsp;20&nbsp;Kč), v&nbsp;mapě vyznačeno lahváčem.
      </td>
    </tr>
    <tr>    
      <td style="width:21%;" valign="top">
          <strong>Časový harmonogram závodu na 24 hodin:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
        <strong>Pátek 25.&nbsp;6.&nbsp;2021:</strong><br>
        18:00–22:00 – prezentace účastníků závodu
        </p>
        <p>
        <strong>Sobota 26.&nbsp;6.&nbsp;2021:</strong><br>
        7:00–9:00 – prezentace účastníků závodu (vč.&nbsp;plombování čipů)<br>
        10:00 – konec plombování čipů a začátek výdeje závodních map<br>
        12:00 – start závodu<br>
        </p>
        <p>
        <strong>Neděle 27.&nbsp;6.&nbsp;2021:</strong><br>
        12:00 – konec závodu<br>
        po 14:00 – předpokládaný začátek vyhlašování vítězů
        </p>
      </td>        
    </tr>
    <tr>    
      <td style="width:21%;" valign="top">
          <strong>Časový harmonogram závodu na 12 hodin:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
        <strong>Pátek 25.&nbsp;6.&nbsp;2021:</strong><br>
        18:00–22:00 – prezentace účastníků závodu
        </p>
        <p>
        <strong>Sobota 26.&nbsp;6.&nbsp;2021:</strong><br>
        7:00–9:00 – prezentace účastníků závodu (vč.&nbsp;plombování čipů)<br>
        9:00 – konec plombování čipů a začátek výdeje závodních map<br>
        10:00 – start závodu<br>
        22:00 – konec závodu<br>
        po 22:30 – předpokládaný začátek vyhlašování vítězů
        </p>
      </td> 
    </tr>
    <tr>         
      <td style="width:21%;" valign="top">
          <strong>Časový harmonogram závodu na 6 hodin:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
        <strong>Sobota 26.&nbsp;6.&nbsp;2021:</strong><br>
        7:00–11:00 – prezentace účastníků závodu (vč.&nbsp;plombování čipů)<br>
        12:00 – konec plombování čipů a začátek výdeje závodních map<br>
        13:00 – start závodu<br>
        19:00 – konec závodu<br>
        po 20:00 – předpokládaný začátek vyhlašování vítězů
        </p> 
        </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Průběh závodu a hodnocení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      	Kontroly jsou v mapě a popisech označeny kódem, přičemž první číslice řádu desítek, vynásobená deseti, udává jejich bodovou hodnotu. Na kontrolách 40 až 49 získáte 40 bodů, kontroly 70 až 79 mají bodovou hodnotu 70 atp. Počet a pořadí navštívených kontrol si volí každý tým dle&nbsp;vlastního uvážení.
        <br>
        Průchod kontrolou se označí vložením čipu do razící jednotky SI (musí bliknout a pípnout). Kontrolní stanoviště je vybaveno oranžovobílým lampionem o&nbsp;velikosti 30×30&nbsp;cm, SI&nbsp;jednotkou a červenobílým plastovým páskem s&nbsp;číslem kontroly a písmenem, které opíšete v&nbsp;případě nefunkčnosti SI ražení.<br>
        O&nbsp;umístění týmu v&nbsp;celkovém pořadí rozhoduje dosažený bodový zisk, při&nbsp;rovnosti bodů nižší dosažený čas. Při&nbsp;překročení časového limitu bude odečteno 20 bodů za&nbsp;každou započatou minutu zpoždění. Při&nbsp;překročení časového limitu o&nbsp;30&nbsp;minut a více nebude tým hodnocen.<br>
        V kategoriích MO, XO a WO se týmy umístěné na&nbsp;prvních dvou místech automaticky kvalifikují na&nbsp;MS&nbsp;2022 a získají finanční podporu ČAR. Vítězové kategorií OPEN obdrží certifikát opravňující k&nbsp;bezplatnému startu na&nbsp;druhém závodě ČAR série 2021, <a href="https://bloudeni.krk-litvinov.cz/2021/cs/" target="_blank">Sudetském&nbsp;Tojnárkovo&nbsp;Bloudění</a> (tým musí závodit ve&nbsp;stejném složení).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Funkcionáři:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Ředitel závodu: Miroslav Seidl<br>
          Stavitel tratí: Zdeněk Rajnošek, Miroslav Seidl<br>
          Hlavní rozhodčí: Jan Tojnar
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Informace:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Na webu: <a href="https://mcr2021.rogaining.cz/" target="_blank">https://mcr2021.rogaining.cz/</a> nebo na&nbsp;mobilu: <strong>739&nbsp;548&nbsp;142</strong> nebo <strong>739&nbsp;327&nbsp;204</strong>
      </td>
    </tr>
        <tr>
      <td style="width:21%;" valign="top">
          <strong>Povinné vybavení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Povinným vybavením je píšťalka a reflexní pásky při&nbsp;pohybu po&nbsp;silničních komunikacích po&nbsp;setmění nebo za&nbsp;snížené viditelnosti.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Upozornění:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <ul>
        <li>Závodí se podle platných pravidel <a href="http://car.shocart.cz/cz/info/pravidla.html" target="_blank">IRF</a>.
        Výjimky z&nbsp;pravidel budou uvedeny na&nbsp;webových stránkách závodu a v&nbsp;pokynech.</li>
        <li>Každý závodník se akce účastní na vlastní nebezpečí a sám zodpovídá za&nbsp;svou bezpečnost a zdravotní stav, což stvrdí svým podpisem prohlášení na&nbsp;prezentaci. Pořadatel neručí za&nbsp;žádné škody způsobené účastníky akce.</li>
        <li>Akce probíhá v&nbsp;CHKO Žďárské vrchy, je nutné dodržovat platná omezení a návštěvní řád CHKO, včetně zákazu vstupu mimo cesty při&nbsp;průchodu přes vyšrafované rezervace (NPR, PR a PP) a klidové zóny zvěře.</li>
        <li>Pokud by epidemiologická opatření nedovolila závod uskutečnit, budeme vracet vložné. O&nbsp;konání/zrušení akce rozhodneme nejpozději v&nbsp;<strong>pondělí 21.&nbsp;6.&nbsp;2021</strong>.</li>
        </ul>
      </td>
      </tr>
      <tr>
      <td style="width:21%;" valign="top">
          <strong>Poděkování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Konání akce umožnil podnik Lesy České republiky, s. p. Poběží se na území, které LČR spravují. <strong>Jsou to i Vaše lesy, chovejte se v&nbsp;nich ohleduplně a tiše.</strong>
      </td>
      </tr>
      <tr>
      <td style="width:21%;" valign="top">
          <strong>Schvalovací doložka:</strong>
      </td>
      <td colspan="4" style="width:79%;">
       	Rozpis byl schválen Prezidiem ČAR v&nbsp;červnu 2021.
      </td>
      </tr>
  </tbody>
</table>