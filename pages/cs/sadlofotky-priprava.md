---
title: Sádlovo fotky z přípravy rogainingového MČR 2021
robots: noindex, nofollow
---

<br>

##### Sádlovy fotky z terénu (jaro 2021)
<div class="figuregroup">
![](../files/sadlofotky/DSC04117.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04118.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04120.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04121.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04122.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04131.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04133.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04135.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04138.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04139.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04141.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04144.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04459.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04460.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04462.jpg){.thumb-sm data-lightbox="o1"}
![](../files/sadlofotky/DSC04466.jpg){.thumb-sm data-lightbox="o1"}
</div>
