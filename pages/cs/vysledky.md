---
title: Výsledky
---

<br>
<br>
<br>

##### Výsledky 25. Mistrovství České republiky v rogainingu

* [Celkové výsledky](/files/results_all.pdf)
* [Mezičasy](/cs/mezicasy.html)

##### Zakreslování postupů na Play-Map:

* [Postupy MČR v rogainingu 2021](https://play-map.com/event.php?id=530)
