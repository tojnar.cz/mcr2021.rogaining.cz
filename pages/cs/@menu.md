[![ČAR](/images/car.gif)](http://www.rogaining.cz/)[![Logo závodu](/files/Logo_4_Palice.png){width=140}](https://mcr2021.rogaining.cz)

[![English website](/images/enflag.png)](/en/)

#### Menu

* [Novinky](/cs/)
* [Výsledky](vysledky.html)
* [Zakreslování postupů](http://play-map.com/event.php?id=530)
* [Fotogalerie](fota.html)
* [Pokyny](pokyny.html)
* [Rozpis](rozpis.html)
* [Přihláškový systém](https://entries.mcr2021.rogaining.cz/cs/)
* [Úvodní upoutávka](/files/leaflet_rogaining_2021.pdf)
* [Informace o počasí](pocasi.html)

#### Další závody ČAR

* [10. Osmistovky Žďárských vrchů (18. 9. 2021)](https://bratrstvo-zdarskych-vrchu.webnode.cz/osmistovky-zdarskych-vrchu/)
* [7. Sudetské Tojnárkovo Bloudění (24.–26. 9. 2021)](https://bloudeni.krk-litvinov.cz/2021/cs/)
* [11. Noční můry (1.–2. 10. 2021)](http://mury.play-map.com/)
* [20. Letní Kraktrek (9.–10. 10. 2021)](http://kraktrek.maweb.eu/)
* [23. Brutus Extreme Orienteering (23.–24. 10. 2021)](http://people.fsv.cvut.cz/www/hora/ob/b/brutus21/)

#### Partneři závodu

[![Obec Křižánky](/images/Logo_Krizanky.jpg){width=120}](http://www.obeckrizanky.cz/) [![Lesy ČR](/images/Logo_LCR.jpg){width=120}](https://lesycr.cz/)

[![O-Mapy](/images/O-Mapy.jpg)](https://www.facebook.com/omapy.cz)   [![Tiskárna Žaket](/images/Zaket.gif)](https://zaket.cz/)

[![Čelovky Lucifer](/images/LogoLucifer.png)](https://www.luciferlights.net/)   [![Playmap – zákresy postupů](/images/playmap.png)](http://play-map.com/)

[![Pivovar Bernard](/images/Bernard_40_8000.jpg){width=120}](https://www.bernard.cz/)   [![Pekařství Marek & Marek Polička](/images/Marek_Marek_CZ.jpg)](https://pekarstvi-marek-marek-sro.business.site/)