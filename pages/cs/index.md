---
title: Novinky
author: Jan Tojnar
---
##### 6. 7. 2021
Uplynul týden od zveřejnění [výsledků](/cs/vysledky.html), žádné protesty neevidujeme, tudíž prohlašujeme za oficiální a neměnné.

##### 1. 7. 2021
25\. MČR v rogainingu se zúčastnilo celkem 163 závodníků z&nbsp;Čech, Slovenska, Polska, Německa a Ruska. 83&nbsp;z&nbsp;nich vystartovalo v&nbsp;mistrovské, 24hodinové kategorii.<br>
Připravil jsem <a href="/files/Map_MCR_2021.jpg">JPG náhled závodní mapy</a> (150 DPI, 2.5 MB), abyste si mohli dokreslit chybějící skály a logo [osmistovek Žďárských vrchů](https://bratrstvo-zdarskych-vrchu.webnode.cz/osmistovky-zdarskych-vrchu/) :-). Na&nbsp;osmistovky, které proběhnou  18. 9. 2021, vás srdečně zvou spolupořadatelé rogainingu z &nbsp;Bratrstva Žďárských vrchů.

##### 30. 6. 2021
Žádná mapa není dokonalá. Občas z ní něco vypadne, třeba louky nebo skály, a někdy jí dokonce do&nbsp;finální podoby musí dopomoci až účastníci závodu. Ti na&nbsp;mapě letošního rogainingu objevili druhou pivní jámu, o&nbsp;které pořadatelé neměli vůbec tušení. Nachází se na&nbsp;modré turistické značce, která vede od&nbsp;obce Javorek na&nbsp;Prosíčka, GPS&nbsp;souřadnice:&nbsp;49.6506700N,&nbsp;16.1797481E, [poloha na mapách.cz](https://mapy.cz/s/renolodalo). Má i svůj [facebook](https://www.facebook.com/Ob%C4%8Derstven%C3%AD-pod-Lominkou-107407078202234). Třeba jí využijete na&nbsp;svých cestách po&nbsp;Vysočině.

##### 29. 6. 2021
Ve [fotogalerii](fota.html) postupně zveřejňujeme fotky od&nbsp;pořadatelů i závodníků. Kdo něco nafotil, může mi poslat odkaz, nebo fota nasdílet přes&nbsp;facebook
[„Máme&nbsp;rádi&nbsp;rogaining“](https://www.facebook.com/groups/233765541508293).

##### 28. 6. 2021
Sláva, i letošní rogainingové mistrovství proběhlo úspěšně. Děkujeme všem účastníkům za&nbsp;přístup a i předvedené výkony. Zveřejňujeme předběžné [výsledky](/cs/vysledky.html). Prosíme o kontrolu, případné chybky opravíme.<br>
Fíša mezitím zpřístupnil zakreslování postupů na Playmaps, můžete nahrávat GPS logy i zakreslovat své víkendové postupy ručně:

* [Postupy MČR v rogainingu 2021](https://play-map.com/event.php?id=530)

##### 23. 6. 2021
Zveřejňujeme [pokyny MČR 2021](/cs/pokyny.html). Všechny [čelovky Lucifer](http://luciferlights.net) jsou už beznadějně vypropůjčeny.<br>
Dnes uzavřeme příjem elektronických přihlášek a začneme balit na&nbsp;rogainingový víkend.<br>
Stále ještě evidujeme pár týmů, které nezaplatily vložné. Prosíme dvojice Počkaj na mňa (? s placením?), Run The World (? už jste doběhli?) a Potkali se u Kolína (doufám, že před pobočkou banky), aby tak neprodleně učinili, popř. nám zaslali potvrzení o provedení platby.

##### 22. 6. 2021
Dnes jsem domluvil víkendovou pohotovost na&nbsp;Vorlově pivní jámě. Její správce tam nově nainstaloval zvon, kterým ho kolemjdoucí mohou přivolat, pokud se zásoby začnou tenčit.<br>
Mapy už máme vytištěny, po nás teď v&nbsp;Žaketu začnou tisknout druhý nejdůležitější závod letošního roku, Mistrovství světa v&nbsp;orientačním běhu :-).  <!-- zítra uzavřeme příjem elektronických přihlášek a zabalíme si na&nbsp;rogainingový víkend. -->

##### 20. 6. 2021
Výběr závodních kontrol je hotov, máme olístečkováno. Na&nbsp;focení nebyl moc čas, přibylo jen [pár snímků](https://photos.app.goo.gl/GWUsmAnrHXDbzYTp8).

Osobně jsem ověřil křižáneckou hospodu Za řekou. Bohužel, v&nbsp;pátek i v&nbsp;sobotu jsem měl smůlu, až do&nbsp;konce června zavírají už ve&nbsp;20:00. Páteční večeři si raději zajistěte cestou, popř. z&nbsp;vlastních zásob. Krámek, obecní cukrárna + prodej zmrzliny na&nbsp;návsi fungují. Bernarda 11° vám po&nbsp;zaplacení natočí pořadatelé.

Dokreslujeme poslední aktualizace mapy, v&nbsp;úterý jí pošleme do&nbsp;tiskárny. Doufám, že do&nbsp;té doby zaplatí vložné i poslední opozdilci.

##### 18. 6. 2021
V některých partiích nabídneme závodníkům hned několik variant občerstvení. Kromě hospod a studánek např. Vorlovu pivní jámu, v mapě značenou [speciálním symbolem lahváče](https://mcr2021.rogaining.cz/images/Pivni_Jama.jpg). Loni v listopadu nejmenovaný dobrodinec v reakci na uzavření restaurací vykopal jámu, umístil do ní bednu, kterou pravidelně doplňuje lahváči piva, popř. i nealko nápoji. Funguje na principu, že dobří lidé ještě nevymřeli. Po zaplacení 20&nbsp;Kč do&nbsp;kasičky si nahodilý kolemjdoucí může vybrat a vypít libovolný nápoj. Po dobu rogainingu máme domluveno posílené zásobování. Více informací na&nbsp;[Vorlově facebooku](https://www.facebook.com/vorlovapivnijama/).

Odvážlivci si mohou i mléko nadojit. Stačí přelézt dvojitý elektrický ohradník a nějak se domluvit s bejkem, který dělá kravičkám společnost :-).

##### 17. 6. 2021
Včera jsem začal potvrzovat přihlášky. Pokud máte pocit, že jsem vaší platbu v&nbsp;[seznamu přihlášených týmů](https://entries.mcr2021.rogaining.cz/cs/team/list) zapomněl odškrtnout, nebo naopak omylem uznal, tak se ozvěte. Týmy, které dosud nezaplatily, nechť tak neprodleně učiní.

Zítra, v pátek 18. 6., končí příjem přihlášek za&nbsp;základní startovné. Ještě pár dní dáme šanci opozdilcům, ale pak už budeme muset objednávku tisku map odeslat do&nbsp;Žaketu. Jelikož účast dle&nbsp;očekávání není zrovna nejvyšší, nezaplacené výtisky si nemůžeme dovolit. Zatím evidujeme něco málo přes sto závodníků, snad do&nbsp;neděle přibydou další.

##### 16. 6. 2021
Na závod si můžete zapůjčit omezený počet čelovek Lucifer M4/M5 a Lucifer&nbsp;S. Více informací o čelovkách (světelný výkon, výdrž) najdete na&nbsp;stránkách výrobce [www.luciferlights.net](https://www.luciferlights.net/).

Dva kusy už byly zamluveny po&nbsp;telefonu, pro dva si rogaineři došli rovnou k&nbsp;Luciferům, takže nyní už zbývá posledních 6&nbsp;kusů. Půjčovné 50&nbsp;Kč se platí na prezentaci, objednávejte prostřednictvím emailu: tojnar@gmail.com. Rozhoduje čas, kdy mi objednávka přistane ve schránce.

##### 15. 6. 2021
<strike>Vláďa Marek, 43 let, hledá parťáka/parťačku pro dvanáctihodinovku. Běhat nechce, chce to obejít pokud možno svižnou chůzí, celkový průměr do&nbsp;5&nbsp;km/h. Zájemci hlaste se na&nbsp;mail pořadatele: tojnar@gmail.com<br>
Už má s kým běžet.</strike>

##### 10. 6. 2021
Ve [fotogalerii](fota.html) přibyly další dva odkazy na fotky z příprav rogainingového závodu.

##### 9. 6. 2021
Abychom byli in, vytvořili jsme [logo závodu](/files/Logo_4_Palice.png) a propagační [leták](/files/leaflet_rogaining_2021.pdf). Asi bychom teď měli uspořádat tiskovou konferenci, a po přinesení předsednictva ho prostřednictvím tiskového mluvčího představit veřejnosti. ČAR má ovšem jen mluvky, kteří se zatím pohybují bez cizí pomoci, a preferují on-line formu prezentace. Každý se teď může pokusit uhodnout, jaký skalní útvar nedaleko závodního centra logo znázorňuje.

##### 4. 6. 2021
Konečně se podařilo přemoci počítačové démony a můžeme spustit [přihláškový systém](https://entries.mcr2021.rogaining.cz/cs/). Ba i [rozpis](https://mcr2021.rogaining.cz/cs/rozpis.html) už máme. Tož se začněte přihlašovat, jinak se vám vykašlu na mapování :-).

##### 26. 5. 2021
O víkendu jsme se Sádlem oběhali pořádný kus závodního prostoru a závazně domluvili shromaždiště. Mně se podařilo i trošku nastydnout, jaro na Vysočině je letos chladné a vlhké. Omrkněte první [fotky z terénu](https://photos.app.goo.gl/xSKrMt7KCUJxDUfu5).

##### 13. 5. 2021
Stejně jako loni, i letos se parta nadšenců pokusí připravit MČR v&nbsp;rogainingu. Mělo by proběhnout o víkendu 26.–27. června na Vysočině. Základní mapu máme již v zásadě hotovou, začínáme vybírat místa vhodná pro kontroly. Zařizujeme povolení (lesáci, CHKO) a zkoušíme oslovit majitele pozemků, použitelných jako shromaždiště.
Doufáme, že do konce června se limity počtů účastníků venkovních akcí ještě navýší, nicméně v krajním případě dokážeme vystartovat závodníky klidně po třídách o 30 žácích. 

Zatím zveřejňujeme klíčové rozhodovací termíny (pouze orientační):

* 31.&nbsp;5.&nbsp;2021 – vyjasnění shromaždiště (kemp se stany nebo hard core start z&nbsp;parkoviště…)
* 1.&nbsp;6.&nbsp;2021 – zveřejnění rozpisu, spuštění přihlášek
* 21.&nbsp;6.&nbsp;2021 – finální rozhodnutí o konání či zrušení závodu a vrácení vložného
