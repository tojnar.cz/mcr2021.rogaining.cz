---
title: <!-- Pokyny 25. Mistrovství České republiky v rogainingu -->
---
<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
  <tbody>
    <tr>
      <td style="width:21%;">
        <center><a href="http://www.rogaining.cz/" target="_blank"><img src="/images/car.gif" alt="Logo ČAR"></a></center>
      </td>
      <td colspan="3" style="width:58%;" align="justify">
        <center><h4>Pokyny 25. Mistrovství České republiky v&nbsp;rogainingu</h4></center>
      </td>
      <td style="width:21%;">
        <center><a href="http://www.caes.cz" target="_blank"><img src="/images/caes-cerna.gif" alt="Logo ČAES"></a></center>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Pořádající orgán:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <a href="http://www.rogaining.cz/" target="_blank">Česká asociace rogainingu a horského orientačního běhu (ČAR)</a>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Pořádající subjekty:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Bratrstvo Žďárských vrchů a
        <br>
        Krušnohorský rogainingový klub Litvínov.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Datum:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <strong>26.–27.&nbsp;červen 2021</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Místo:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <strong>Křižánky</strong>, okres Žďár nad Sázavou, Česká republika
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Centrum závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <a href="https://mapy.cz/s/lalubejavo" target="_blank">Hřiště u hřbitova</a>; GPS:&nbsp;N49.6841542°, E16.0627439°.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Druh závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Mistrovství České republiky v&nbsp;rogainingu na&nbsp;24&nbsp;hodin (sobota 12:00 hod. až neděle 12:00 hod.)</li>
          <li>Předkvalifikační závod pro&nbsp;MS v&nbsp;rogainingu v&nbsp;roce 2022</li>
          <li>1. závod ČAR série 2021</li>
          <li>Veřejný rogainingový závod na 12 hodin (sobota 10:00 až 22:00 hod.)</li>
          <li>Veřejný rogainingový závod na 6 hodin (sobota 13:00 až 19:00 hod.)</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Prostor závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Prostor závodu je vymezen obcemi Hlinsko, Borová, Jimramov, Skalský Dvůr, Nové Město na Moravě, Vysoké a Vojnův Městec.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Terén:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Zvlněná krajina pahorkatiny Žďárských vrchů, nadmořská výška 600–836&nbsp;m. Středně hustě osídlená oblast s rozsáhlou sítí dopravních komunikací a cest. Krajina je místy zemědělsky využívána (pole, pastviny...).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Nástrahy na trati:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Dbejte prosím zvýšené opatrnosti při&nbsp;pohybu na&nbsp;silničních komunikacích, o&nbsp;víkendu zde panuje silný provoz. Zákonem předepsaná povinnost nosit na&nbsp;silnici po&nbsp;setmění reflexní pásky platí i pro&nbsp;náš závod.</li>
          <li>V mapě se vyskytují četné skalní srázy, dejte si pozor zejména u&nbsp;kontrol 67, 72, 82 a 91, hlavně v&nbsp;noci.</li>
          <li>V&nbsp;závodním prostoru se nachází velký počet rezervací a klidových zón zvěře. V&nbsp;mapě jsou vyznačeny šikmými růžovými šrafami, průchod přes ně je povolen pouze po&nbsp;cestách znázorněných v&nbsp;mapě.</li>
          <li>Ovocné sady, zahrady, obory, pastviny a některé další soukromé pozemky, na&nbsp;kterých hrozí kolize s&nbsp;býky nebo majitelem, jsou v&nbsp;mapě vyznačeny fialovými křížovými šrafami. Vyhnout byste se měli i olivově zeleným plochám soukromých pozemků. Vždy dbejte na&nbsp;to, abyste svým pohybem přes evidentně upravované pozemky kolem obydlí a chat nenarušovali soukromí druhých osob.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Mapa:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Speciálka pro rogaining, měřítko 1: 40 000, e&nbsp;=&nbsp;10&nbsp;m, stav 06/2021, rozměr 530x450 mm, vodovzdorný materiál Pretex.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Zvláštní symboly:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <a href="/files/Zvlastni_Symboly.jpg" target="_blank"><img src="/files/Zvlastni_Symboly.jpg" alt="Zvláštní symboly" width="350"></a>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Popisy:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Popisy kontrol, včetně jejich bodové hodnoty, vytištěné v češtině a angličtině, budou rozdávány spolu se&nbsp;závodními mapami.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Podmínky účasti:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Všichni závodníci budou na prezentaci podepisovat prohlášení, že startují na&nbsp;vlastní nebezpečí, nejeví známky nakažení COVID 19, ani nepřišli do&nbsp;styku s&nbsp;osobou infikovanou nebo v&nbsp;karanténě, popř. onemocnění COVID prodělali před maximálně 180 dny, či mají ukončené očkování nejpozději před 22 dny. Za&nbsp;účastníky mladší 18 let toto prohlášení podepíše jejich rodič nebo zákonný zástupce.</li>
          <li>Závodníci mladší 15 let se mohou zúčastnit pouze v&nbsp;doprovodu osoby starší 18&nbsp;let</li>
          <li>MČR na 24 hodin se smí zúčastnit pouze sportovci registrovaní v&nbsp;ČAR (týká se všech členů týmu). Bezplatná registrace je možná na&nbsp;prezentaci.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Vypsané závody a kategorie:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>MČR na 24 hodin (MY, WY, XY, MO, WO, XO, MV, WV, XV, MSV, WSV, XSV)</li>
          <li>Veřejný závod na 12 hodin (MO12, WO12, XO12)</li>
          <li>Veřejný závod na 6 hodin (MO6, WO6, XO6)</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Doprava:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Vlastními dopravními prostředky nebo hromadnou dopravou.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Parkování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Zdarma, pouze na vymezených plochách v&nbsp;blízkosti centra, bude značeno a řízeno pořadateli. Do&nbsp;areálu závodního centra je vjezd přísně zakázán.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Ubytování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        V centru závodu na hřišti ve vyhrazeném prostoru je možné postavit vlastní stany, viz plánek centra:<br><a href="/files/Zavodni_Centrum.jpg" target="_blank"><img src="/files/T_Zavodni_Centrum.jpg" alt="plánek centra" width="350"></a><br>
        Závodníci na 12 a 24 hodin mají nocleh zahrnut v ceně startovného.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Občerstvení v závodním centru:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        V centru závodu budeme provozovat pořadatelský bufet s&nbsp;omezeným sortimentem (točené pivo, nealko pivo, nealko nápoje, káva, čaj, pečivo z místní pekárny...) + teplá voda. Závodníci dvanáctihodinovky a čtyřiadvacetihodinovky budou mít od&nbsp;18:00 k&nbsp;dispozici základní hash house. Večer možná zažehneme ohýnek na&nbsp;zahřátí.<br>
        Bufet i hash house chceme provozovat nízko odpadově a šetřit přírodu tím, že nebudeme podporovat používání jednorázových plastových nádob a kelímků. Dovezte si s&nbsp;sebou i potřebné vybavení (půllitry, kalíšky, misky, příbory apod.).
        <br>
        Restaurace v&nbsp;Křižánkách zavírají v&nbsp;pátek už ve&nbsp;20:00, povečeřte raději po&nbsp;cestě, nebo z&nbsp;vlastních zásob.<br>
        V sobotu dopoledne by na návsi měla být otevřena Obecní kavárna a obchod.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Občerstvení během závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Pitnou vodu zajišťuje pořadatel pouze v závodním centru.  V terénu najdete dostatek studánek, vodu je možné načerpat také z&nbsp;lesních bystřin, je ale třeba dát pozor, aby nad místem odběru neležela nějaká pastvina. Závodní prostor oplývá relativně velkým počtem restaurací a hospod. Budou zakresleny do&nbsp;mapy s&nbsp;tím, že funkčnost podniku nezaručujeme. Závodníci mohou kromě nich využít i zvláštní občerstvovačku (pivo nebo nealko za&nbsp;20&nbsp;Kč), v&nbsp;mapě vyznačeno lahváčem.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Hygienická opatření, WC a mytí:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Věříme ve&nbsp;Váš zdravý rozum a předpokládáme, že kdybyste představovali zdravotní riziko pro&nbsp;své okolí, tak start oželíte. Mějte na&nbsp;paměti, že dost rogainerů věkově spadá do&nbsp;rizikové skupiny. Dodržujte prosím dostatečné rozestupy.<br>
        V centru budou k&nbsp;dispozici mobilní WC, vybavené toaletním papírem a dezinfekcí.<br>
        Centrum závodu se nachází uprostřed CHKO Žďárské vrchy, veškeré odpadky si prosím, buď odneste, nebo je odložte do&nbsp;pořadatelských odpadkových košů.<br>
        K mytí využijte nedaleký rybník.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Časový harmonogram závodu na 24 hodin:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
          <strong>Pátek 25.&nbsp;6.&nbsp;2021:</strong><br>
          18:00–22:00 – prezentace účastníků závodu
        </p>
        <p>
          <strong>Sobota 26.&nbsp;6.&nbsp;2021:</strong><br>
          7:00–9:00 – prezentace účastníků závodu (vč.&nbsp;plombování čipů)<br>
          10:00 – konec plombování čipů a začátek výdeje závodních map<br>
          12:00 – start závodu<br>
        </p>
        <p>
          <strong>Neděle 27.&nbsp;6.&nbsp;2021:</strong><br>
          12:00 – konec závodu<br>
          po 14:00 – předpokládaný začátek vyhlašování vítězů
        </p>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Časový harmonogram závodu na 12 hodin:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
          <strong>Pátek 25.&nbsp;6.&nbsp;2021:</strong><br>
          18:00–22:00 – prezentace účastníků závodu
        </p>
        <p>
          <strong>Sobota 26.&nbsp;6.&nbsp;2021:</strong><br>
          7:00–9:00 – prezentace účastníků závodu (vč.&nbsp;plombování čipů)<br>
          9:00 – konec plombování čipů a začátek výdeje závodních map<br>
          10:00 – start závodu<br>
          22:00 – konec závodu<br>
          po 22:30 – předpokládaný začátek vyhlašování vítězů
        </p>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Časový harmonogram závodu na 6 hodin:</strong>
      </td>
      <td colspan="4" style="width:79%;" valign="top">
        <p>
          <strong>Sobota 26.&nbsp;6.&nbsp;2021:</strong><br>
          7:00–11:00 – prezentace účastníků závodu (vč.&nbsp;plombování čipů)<br>
          12:00 – konec plombování čipů a začátek výdeje závodních map<br>
          13:00 – start závodu<br>
          19:00 – konec závodu<br>
          po 20:00 – předpokládaný začátek vyhlašování vítězů
        </p>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Systém ražení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Elektronický systém SportIdent.<br>
        <strong>!!! Každý člen týmu musí mít svůj SI čip !!!</strong><br>
        Pro závody na 12 a 24 hodin doporučujeme použít pouze čipy se&nbsp;zvýšenou kapacitou. Pokud se přihlásíte s&nbsp;nízkokapacitním čipem (30 kontrol u&nbsp;SI&nbsp;5&nbsp;a&nbsp;8 nebo 50 kontrol u&nbsp;SI&nbsp;9), nezapočítáme vám žádné kontroly kromě těch zaznamenaných v&nbsp;čipu. Při ztrátě zapůjčeného čipu zaplatíte částku 2000 Kč.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Průběh závodu a hodnocení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Kontroly jsou v mapě a popisech označeny kódem, přičemž první číslice řádu desítek, vynásobená deseti, udává jejich bodovou hodnotu. Na kontrolách 40 až 49 získáte 40 bodů, kontroly 70 až 79 mají bodovou hodnotu 70 atp. Počet a pořadí navštívených kontrol si volí každý tým dle&nbsp;vlastního uvážení.
        <br>
        Jednotlivé kategorie vystartují z louky na shromaždišti podle časového harmonogramu. Před startem jsou závodníci povinni vynulovat a zkontrolovat si své SI čipy pomocí krabiček Clear a Check. Pořadatelé když tak poradí.<br>
        Kontroly budou aktivovány v&nbsp;AIR+ módu. Průchod kontrolou se označí vložením čipu do&nbsp;razící jednotky SI (musí bliknout a pípnout). Kontrolní stanoviště je vybaveno oranžovobílým lampionem o&nbsp;velikosti 30×30&nbsp;cm, SI&nbsp;jednotkou a červenobílým plastovým páskem s&nbsp;trojciferným kódem kontroly, který opíšete v&nbsp;případě nefunkčnosti SI ražení.<br>
        Cíl bude umístěn na stejném místě jako start. Ihned po doběhu je každý závodník povinen orazit cílovou SI jednotku a poté si nechat vyčíst čip ve vyčítací jednotce ve stánku prezentace. I v případě, že závod vzdáte, musíte projít cílem a nahlásit se pořadatelům, aby nebylo nutno vyhlašovat celostátní pátrání!<br>
        O&nbsp;umístění týmu v&nbsp;celkovém pořadí rozhoduje dosažený bodový zisk, při&nbsp;rovnosti bodů nižší dosažený čas. Při&nbsp;překročení časového limitu bude odečteno 20 bodů za&nbsp;každou započatou minutu zpoždění. Při&nbsp;překročení časového limitu o&nbsp;30&nbsp;minut a více nebude tým hodnocen.<br>
        V kategoriích MO, XO a WO se týmy umístěné na&nbsp;prvních dvou místech automaticky kvalifikují na&nbsp;MS&nbsp;2022 a získají finanční podporu ČAR. Vítězové kategorií OPEN obdrží certifikát opravňující k&nbsp;bezplatnému startu na&nbsp;druhém závodě ČAR série 2021, <a href="https://bloudeni.krk-litvinov.cz/2021/cs/" target="_blank">Sudetském&nbsp;Tojnárkovo&nbsp;Bloudění</a> (tým musí závodit ve&nbsp;stejném složení).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Výňatek z pravidel a doplnění:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Závodí se podle platných pravidel <a href="http://car.shocart.cz/cz/info/pravidla.html" target="_blank">IRF</a>.</li>
          <li>Všichni členové týmu musí absolvovat celý závod pohromadě (na dohled). V prostoru závodu se budou pohybovat rozhodčí s foťákem a fixlující dvojice budou diskvalifikovány.</li>
          <li>Na jednotlivých kontrolních bodech musí celý tým stihnout orazit v časovém intervalu 60 sekund. Při překročení minutového limitu na oražení nebudou body za průchod kontrolou započítány (kontrolováno automaticky programem).</li>
          <li>Každý závodní tým smí při závodě využít pouze mapy poskytnuté pořadatelem a pohybovat se pomocí vlastních sil. Je povoleno zaznamenávat trasu pomocí GPS trackeru, ale nesmíte využívat aktivní navigační zařízení GPS (s displejem), ani cestovat jakýmikoli dopravními prostředky (koloběžka, kolo, motocykl, auto, veřejná doprava, zvíře atp.).</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Funkcionáři:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Ředitel závodu: Miroslav Seidl<br>
        Stavitel tratí: Zdeněk Rajnošek, Miroslav Seidl<br>
        Hlavní rozhodčí: Jan Tojnar
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Informace:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Na webu: <a href="https://mcr2021.rogaining.cz/" target="_blank">https://mcr2021.rogaining.cz/</a> nebo na&nbsp;mobilu: <strong>739&nbsp;548&nbsp;142</strong> nebo <strong>739&nbsp;327&nbsp;204</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Upozornění:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <ul>
          <li>Každý závodník se akce účastní na vlastní nebezpečí a sám zodpovídá za&nbsp;svou bezpečnost, což stvrdí svým podpisem prohlášení na&nbsp;prezentaci. Pořadatel neručí za&nbsp;žádné škody způsobené účastníky nebo účastníkům akce.</li>
          <li>Povinným vybavením je píšťalka a reflexní pásky při&nbsp;pohybu po&nbsp;silničních komunikacích po&nbsp;setmění.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Poděkování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Děkujeme obci Křižánky za poskytnutí prostor pro závodní centrum a plochy pro parkování. Dále děkujeme vlastníkům a správcům rozsáhlých lesních prostor, kteří umožnili konání této sportovní akce:
        <ul>
          <li>Lesy České republiky, státní podnik</li>
          <li>KINSKÝ Žďár, a.s.</li>
          <li>Belcredi Ludvík PhDr. a Správa lesů Jimramov</li>
          <li>Biskupství královéhradecké a Diecézní lesy Hradec Králové s.r.o.</li>
          <li>Město Polička, jako vlastník a správce městských lesů</li>
        </ul>
        Akce se koná na území, které z velké části spravuje státní podnik Lesy České republiky. Jsou to i Vaše lesy, chovejme se v&nbsp;nich ohleduplně a tiše.
      </td>
    </tr>
  </tbody>
</table>