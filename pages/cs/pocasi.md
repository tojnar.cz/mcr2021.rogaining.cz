---
title: Informace o počasí v závodním prostoru
---
##### Předpověď počasí
* [Norská předpověď pro Křižánky](https://www.yr.no/en/forecast/daily-table/2-11925577/Czech%20Republic/Kraj%20Vyso%C4%8Dina/%C5%BD%C4%8F%C3%A1r%20nad%20S%C3%A1zavou%20District/K%C5%99i%C5%BE%C3%A1nky)
* [Foreca weather Křižánky](https://www.foreca.cz/Czech-Republic/Pardubick%C3%BD-kraj/K%C5%99%C3%AD%C5%BE%C3%A1nky)
